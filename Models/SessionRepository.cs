
using System;

namespace AuthenApi.Models
{
    public class SessionRepository : ISessionRepository
    {
        private readonly AuthenContext _context;

        public SessionRepository(AuthenContext context)
        {
            _context = context;
        }
        public Session Add(string username, string password)
        {
            User auser = null;
            Console.WriteLine(username + ":" + password);
            foreach(User user in _context.Users) {
                Console.WriteLine(user.Name + ":" + user.Password);
                if(user.Name == username && user.Password == password) {
                    auser = user;
                    break;
                }
            }
            if(auser == null) return null;
            Session session = new Session();
            session.UserID = auser.UserID;
            _context.Sessions.Add(session);
            _context.SaveChanges();
            return session;
        }

        public Session Find(int key)
        {
            return _context.Sessions.Find(key);
        }

        public Session Remove(int key)
        {
            Session session = _context.Sessions.Find(key);
            _context.Sessions.Remove(session);
            _context.SaveChanges();
            return session;
        }
    }
}